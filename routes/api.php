<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


/*
|--------------------------------------------------------------------------
| Routes to implements rest crud for employees
|--------------------------------------------------------------------------
*/

/*
 * Receive all employees
 */
Route::get('/employees','Api\Employee\EmployeeController@getEmployees');

Route::get('/subordinated','Api\Employee\EmployeeController@getSubordinated');

Route::get('/subordinated/{superior}','Api\Employee\EmployeeController@getSubordinated');

Route::get('/positions','Api\Employee\EmployeeController@getPositions');

Route::get('/employees/{employee}','Api\Employee\EmployeeController@getEmployee');

Route::patch('/employees/{employee}','Api\Employee\EmployeeController@updateEmployee');

Route::post('/employees/{employee}/avatar','Api\Employee\EmployeeController@updateEmployeeAvatar');

Route::delete('/employees/{employee}/delete','Api\Employee\EmployeeController@deleteEmployee');
