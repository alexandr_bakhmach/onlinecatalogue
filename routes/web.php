<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();



/*
|--------------------------------------------------------------------------
| Task routes
|--------------------------------------------------------------------------
|
| Registered routes specified for test task.
| Current routes are responsible for returning main of two tasks pages
|
*/

Route::get('/task1','Web\HomeController@getTask1Page')->name('task1');
Route::get('/task2','Web\HomeController@getTask2Page')->name('task2');