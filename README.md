# INSTALATION #

$ git clone <repo>

$ cd <repo_folder>

$ composer install

$ npm install

Then create db and add it to .env file or use .env.example and the db dump 

$ php artisan  migrate

$ php artisan key:generate

$ php artisan db:seed

$ npm run prod
