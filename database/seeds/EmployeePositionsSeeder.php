<?php

use App\Models\EmployeePosition;
use Illuminate\Database\Seeder;

class EmployeePositionsSeeder extends Seeder
{
    const POSITIONS = [
        'habrahabr read developer',
        'stackoverflow copypaste developer',
        'your code comment developer',
        'do nothing developer',
        'eat developer',
        'kfc lets go developer',
        'kfc lets go lead',
        'stackoverflow copypaste lead',
        'sleep lead',
        'sleep developer',
        'why my code do that !!? developer'
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (self::POSITIONS as $reallyTruePosition) {
            EmployeePosition::create([
                'position' => $reallyTruePosition
            ]);
        }
    }


}
