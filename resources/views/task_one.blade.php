@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="wrapper">
                    <div class="container">
                        <div class="raw">
                            <div class="col-md-8 col-md-offset-2">
                                <ul class="list-group employee-list" id="main-employee-list">
                                </ul>
                                <div class="col-md-12">
                                    <ul id="employee-pagination" class="pagination-sm"></ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @include('scripts.task_one_scripts')
@endsection