<script>

    let subordinatedUrl = '/api/subordinated';
    let employeeList = $('.employee-list');
    let $pagination = $('#employee-pagination');

    let paginate = function(event, page) {
        loadSubordinatedOnInit($('#main-employee-list'),page,event);
    };

    let defaultOpts = {
        initiateStartPageClick: false,
        onPageClick: paginate
    };

    let loadSubordinated = function (target, callback = null, page = null, event = null) {
        let id = (target ? target.data('id') : null);
        $.ajax({
            url: (id ? subordinatedUrl + '/' + id : subordinatedUrl),
            type: "GET",
            data: {
                page: page
            },
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(response) {
                if(callback) {
                    callback(response, page, event,target);
                }
            },
            error: function(xhr) {
                console.log(xhr);
            }
        });
    };

    let loadSubordinatedOnClick = function(e) {
        console.log(e.target);
        let element = $(e.target);
        let parent = element.find('.employee-subordinated-ul');
        if(!parent.length > 0) {
            loadSubordinated($(e.target), (response, page, event, target) => {
                if(response.length > 0) {
                    let container = $('<ul class="pagination-sm employee-subordinated-ul"></ul>');
                    target.append(container);
                    response.forEach((item) => {
                        let id = 'employee-subordinated-' + item.id;
                        let element = $('<li data-id=' + item.id + ' class="list-group-item" id="'+id+'">' + item.first_name + '</li>');
                        container
                            .append(element);
                        element.on('click','#' + id,loadSubordinatedOnClick);
                    });
                }
            });
        }else{
            if(parent.css('display') === 'none'){
                parent.css('display','inherit')
            }else{
                parent.css('display','none')
            }
        }
    };

    let loadSubordinatedOnInit = function(selector, page = 1, event = null) {
        loadSubordinated(selector, (response, page) => {
            selector.html('');
            response.data.forEach((item) => {
               selector.append('<li data-id=' + item.id + ' class="list-group-item" id="employee-subordinated-main">' + item.first_name + '</li>');
            });
            let totalPages = (response.last_page === 0) ? 1 : response.last_page;
            let currentPage = page;
            $pagination.twbsPagination('destroy');
            $pagination.twbsPagination($.extend({}, defaultOpts, {
                startPage: currentPage,
                totalPages: totalPages
            }));
        }, page, event)
    };


    $( document ).ready(function() {
        $pagination.twbsPagination(defaultOpts);
        loadSubordinatedOnInit($('#main-employee-list'));
        employeeList.on('click','#employee-subordinated-main',loadSubordinatedOnClick);
    });

</script>
