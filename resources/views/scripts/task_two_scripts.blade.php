<script>

    'use strict';

    let getEmployeesUrl = '/api/employees';
    const getPositionsUrl = '/api/positions';
    let preloader = null;
    let inputField = $('#search');
    let sending = false;
    let $pagination = $('#employee-pagination');
    let employeeTable = $('#employees-table');
    let saveBtn = $('#save-btn');
    let superiorSelect = $('#superior-select');
    let clearSuperiorBtn = $('#clear-superior-btn');


    let getEmployeeInfo = function (e) {
        let id = $(e.target).closest('tr').data('id');
        $.ajax({
            url: '/api/employees/'  + id,
            type: "GET",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                let position = (response.position ? response.position.id : -1);
                superiorSelect.append(response.superior
                    ? "<option value='"+response.superior.id + "'selected>" +
                    response.superior.first_name + '  ' +
                    response.superior.last_name + "</option>"
                    : '<option value="-1" selected disabled>Choose chuvachella</option>'
                );
                superiorSelect.trigger('change');
                $('#profile').data('id',id);
                $('#profile #first-name').val(response.first_name);
                $('#profile #last-name').val(response.last_name);
                $('#profile #fathers-name').val(response.fathers_name);
                $('#profile #salary').val(response.salary);
                $('#profile #w-start-time').val(response.w_start_time);
                $('#profile #positions').val(position);
                $('#profile').modal('show');
            },
            error: function (xhr) {
                console.log(xhr);
                $('#no-profile').modal('show');
            }
        });
    };

    let addParameterToURL = function(key, value, oldURL){
        let url = oldURL;
        url += (url.split('?')[1] ? '&':'?') + key + '=' + value;
        return url;
    };

    let seedTable = function(selector,data) {
      let html = "";
      data.forEach(function(item) {
          html += `<tr class="employee-raw" data-id=` + item.id + `>
                    <td>` + item.first_name + `</td>
                    <td>` + item.last_name + `</td>
                    <td>` + item.fathers_name + `</td>
                    <td>` + item.salary + `</td>
                    <td>` + item.w_start_time + `</td>
                    <td>` + (item.position ? item.position.position : 'No position') + `</td>
                    <td>` + (item.superior ? item.superior.first_name + ' ' +
                                             item.superior.last_name + ' ' +
                                             item.superior.fathers_name : 'No superior') + `</td>
                    <td>
                      <div class="row">
                      <div class="col-md-12">
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <button type="button" class="btn btn-primary edit-btn">
                              Edit
                            </button>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <button type="button" class="btn btn-danger delete-btn">
                              Delete
                            </button>
                        </div>
                      </div>
</div>
                    </td>
                    </tr>`;
      });
      $(selector).html(html);
    };

    let paginate = function(event, page){
        if(!sending && page >= 1) {
            $('#employees-table tbody').empty();
            sending = true;
            preloader.css('display', 'inherit');
            setTimeout(function () {
                $.ajax({
                    url: getEmployeesUrl,
                    data: {
                        page: page
                    },
                    type: "GET",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        sending = false;
                        preloader.css('display', 'none');
                        seedTable('#employees-table tbody', response.data);
                        let totalPages = (response.last_page == 0) ? 1 : response.last_page;
                        let currentPage = page;
                        $pagination.twbsPagination('destroy');
                        $pagination.twbsPagination($.extend({}, defaultOpts, {
                            startPage: currentPage,
                            totalPages: totalPages
                        }));
                    },
                    error: function (xhr) {
                        console.log(xhr);
                    }
                });
            }, 1000);
        }
    };

    let defaultOpts = {
        initiateStartPageClick: false,
        onPageClick: paginate
    };

    let search = function(e){
        if(!sending) {
            getEmployeesUrl = addParameterToURL('s', $(e.target).val(), '/api/employees');
            paginate(null, 1);
        }
    };

    let loadPositions = function(){
      let positionSelect = $('#positions');

        $.ajax({
            url: getPositionsUrl,
            type: "GET",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(response) {
                response.forEach(function (item) {
                    positionSelect.append($('<option>', {
                        value: item.id,
                        text: item.position
                    }).data('id',item.id))
                });
            },
            error: function(xhr) {
                console.log(xhr);
            }
        });
    };

    let updateEmpoyee = function(e){
        let id = $(e.target).closest('#profile').data('id');
        let data = {
            first_name: $('#profile #first-name').val(),
            last_name: $('#profile #last-name').val(),
            fathers_name: $('#profile #fathers-name').val(),
            salary: $('#profile #salary').val(),
            w_start_time: $('#profile #w-start-time').val(),
            position_id: ($('#profile #positions').val() == -1 ? null : $('#profile #positions').val()),
            superior_id: $('#profile #superior-select').val()
        };
        console.log(data);
        $.ajax({
            url: '/api/employees/' + id,
            type: "PATCH",
            data: data,
            contentType: "application/x-www-form-urlencoded",
            dataType: "json",
            success: function(response, textStatus, xhr) {
                $('#profile').modal('toggle');
                $('#alert-modal #alert').removeClass();
                $('#alert-modal #alert').addClass('alert alert-success');
                $('#alert-modal #pre-alert').text('Success!');
                $('#alert-modal #alert-msg').text('Employee was successfully updated.');
                $('#alert-modal').modal('show');
                paginate(null,1);
                console.log('asd');
            },
            error: function(xhr,sd,d) {
                console.log(d);
            }
        });
    };

    let deleteEmployee = function(e) {
        let id = $(e.target).closest('tr').data('id');
        $.ajax({
            url: '/api/employees/' + id + '/delete',
            type: "DELETE",
            contentType: "application/x-www-form-urlencoded",
            dataType: "json",
            success: function(response) {
                console.log(response);
                paginate(null,1);
            },
            error: function(xhr,sd,d) {
                console.log(d);
            }
        });
    };

    let clearSuperior = function() {
        superiorSelect.append('<option value="-1" selected disabled>Choose chuvachella</option>');
        superiorSelect.trigger('change');
    };

    /*
    | Load all employee list while document
    | is be ready to render it
     */
    $( document ).ready(function() {
        loadPositions();
        inputField.on('input',search);
        saveBtn.on('click',updateEmpoyee);
        employeeTable.on('click','.edit-btn',getEmployeeInfo);
        employeeTable.on('click','.delete-btn',deleteEmployee);
        clearSuperiorBtn.on('click',clearSuperior);
        superiorSelect.select2({
            width: '100%',
            ajax: {
                url: '/api/employees',
                data: function (params) {
                    return {
                        s: params.term,
                    };
                },
                processResults: function (data) {
                    console.log(data);
                    let results = [];
                    data.data.forEach(function(item) {
                        results.push({
                            id: item.id,
                            text: item.first_name + ' ' + item.last_name
                        });
                    });
                    return {
                        results: results
                    };
                },
            }
        });
        $pagination.twbsPagination(defaultOpts);
        preloader = $('.sk-circle');
        paginate(null,1);
    });

</script>