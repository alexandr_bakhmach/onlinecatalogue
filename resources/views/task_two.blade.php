@extends('layouts.app')

@section('content')
    <!-- Modal -->
    <div class="modal fade" id="no-profile" tabindex="-1" role="dialog" aria-labelledby="profile-lable" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="profile-label">Profile</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h4>No such user</h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>



    <div class="modal fade" id="alert-modal" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Alert!</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-warning" id="alert">
                        <strong id="pre-alert">Alert!</strong> <span id="alert-msg"></span>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="profile" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <label class="modal-title" id="profile-label">Profile</label>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="employee-form">
                        <div class="form-group">
                            <label for="first-name">First name:</label>
                            <input type="text" class="form-control" id="first-name">
                        </div>
                        <div class="form-group">
                            <label for="last-name">Last name:</label>
                            <input type="text" class="form-control" id="last-name">
                        </div>
                        <div class="form-group">
                            <label for="fathers-name">Fathers name:</label>
                            <input type="text" class="form-control" id="fathers-name">
                        </div>
                        <div class="form-group">
                            <label for="salary">Salary:</label>
                            <input type="number" class="form-control" id="salary">
                        </div>
                        <div class="form-group">
                            <label for="w-start-time">Work start time:</label>
                            <input type="date" class="form-control" id="w-start-time">
                        </div>
                        <div class="form-group">
                            <label for="positions">Position:</label>
                            <select class="form-control" id="positions">
                                <option value="-1">Choose position</option>
                            </select>
                        </div>
                        <div class="form-group superior-group" id="superior-container">
                            <label for="superior-select">Superior:</label>
                            <a class="btn btn-success" id="clear-superior-btn">Clear</a>
                            <select id="superior-select" class="form-control">
                                <option value="-1" selected disabled>Choose chuvachella</option>
                            </select>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">

                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="save-btn">Save changes</button>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-12">
                    <form class="form-inline" >
                        <div class="form-group pull-right">
                            <label for="search">Search :</label>
                            <input type="text" class="form-control" id="search">
                        </div>
                    </form>
                </div>
                <div class="table-responsive col-md-12">
                    <table class="table" id="employees-table">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col">First name</th>
                            <th scope="col">Last name</th>
                            <th scope="col">Fathers name</th>
                            <th scope="col">Salary</th>
                            <th scope="col">Start work time</th>
                            <th scope="col">Position</th>
                            <th scope="col">Superior</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    <div class="sk-circle">
                        <div class="sk-circle1 sk-child"></div>
                        <div class="sk-circle2 sk-child"></div>
                        <div class="sk-circle3 sk-child"></div>
                        <div class="sk-circle4 sk-child"></div>
                        <div class="sk-circle5 sk-child"></div>
                        <div class="sk-circle6 sk-child"></div>
                        <div class="sk-circle7 sk-child"></div>
                        <div class="sk-circle8 sk-child"></div>
                        <div class="sk-circle9 sk-child"></div>
                        <div class="sk-circle10 sk-child"></div>
                        <div class="sk-circle11 sk-child"></div>
                        <div class="sk-circle12 sk-child"></div>
                    </div>
                </div>
                <div class="col-md-12">
                    <ul id="employee-pagination" class="pagination-sm"></ul>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @include('scripts.task_two_scripts')
@endsection