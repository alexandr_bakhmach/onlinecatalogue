<?php
/**
 * Created by PhpStorm.
 * User: alexandr
 * Date: 26.01.18
 * Time: 11:17
 */

namespace App\Services;


use App\Http\Requests\Api\UpdateEmployeeRequest;
use App\Models\Employee;
use Illuminate\Http\JsonResponse;

class EmployeeService
{

    /**
     * This http status code tels that
     * the object was fully successfully created
     */
    const CREATED = 201;

    /**
     *
     * Handle update employee request
     * Check if employee can has the passed
     * in request superior
     *
     * @param UpdateEmployeeRequest $request
     * @param Employee $employee
     * @return JsonResponse
     */
    public function handleEmployeeUpdate(UpdateEmployeeRequest $request,
                                         Employee $employee)
    : JsonResponse
    {

        $employee
            ->update($request->only('first_name','last_name','fathers_name',
            'w_start_time','salary','position_id','superior_id'));

        return response()->json($employee);

    }
}