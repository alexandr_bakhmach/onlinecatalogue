<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{


    /**
     * Return the view for task1
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getTask1Page()
    {
        return View('task_one');
    }

    /**
     *
     * Return the view for task2
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getTask2Page()
    {
        return View('task_two');
    }
}
