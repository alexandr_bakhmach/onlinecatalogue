<?php

namespace App\Http\Controllers\Api\Employee;

use App\Http\Requests\Api\GetEmployeeRequest;
use App\Http\Requests\Api\UpdateEmployeeAvatarRequest;
use App\Http\Requests\Api\UpdateEmployeeRequest;
use App\Models\Employee;
use App\Http\Controllers\Controller;
use App\Models\EmployeePosition;
use App\Services\EmployeeService;

class EmployeeController extends Controller
{
    public function getEmployees(GetEmployeeRequest $request)
    {
        $employees = Employee
            ::when($word = $request->get('s'), function ($query) use ($word) {
                return $query->where('first_name','LIKE', '%' . $word . '%');
            })
            ->orderBy('id')
            ->with(['position','superior'])
            ->paginate(7);

        return response()->json($employees);
    }

    public function getPositions()
    {
        $positions = EmployeePosition::orderBy('id')->get();
        return response()->json($positions);
    }

    public function getEmployee(Employee $employee)
    {
        return response()->json($employee
            ->load(['position','superior']));
    }

    public function updateEmployee(UpdateEmployeeRequest $request,
                                   Employee $employee,
                                   EmployeeService $employeeService)
    {
        return $employeeService->handleEmployeeUpdate($request,$employee);
    }

    public function deleteEmployee(Employee $employee)
    {
        $subordinated = $employee->subordinated();

        $superior = $employee->superior();

        foreach ($subordinated as $sub) {
            $sub->superior()->save($superior);
        }

        $employee->delete();

        return response()->json(true);
    }

    public function getSubordinated(Employee $superior)
    {
        if($superior->id) {
            return response()->json($superior
                ->subordinated()
                ->orderBy('id')
                ->get());
        } else {
            return response()->json(Employee::whereNull('superior_id')
                ->orderBy('id')
                ->paginate());
        }
    }
}
