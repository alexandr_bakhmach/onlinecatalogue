<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class UpdateEmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'string|max:60|min:2|nullable',
            'last_name' => 'string|max:60|min:2|nullable',
            'fathers_name' => 'string|max:60|min:2|nullable',
            'w_start_time' => 'date|nullable',
            'position_id' => 'exists:employee_positions,id|nullable',
            'superior_id' => 'exists:employees,id|nullable',
            'salary' => 'numeric|nullable'
        ];
    }
}
