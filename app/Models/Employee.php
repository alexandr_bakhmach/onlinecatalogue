<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $table = 'employees';

    protected $fillable = ['id','created_at','updated_at',
        'first_name','last_name','fathers_name','position_id',
        'superior_id','subordinated_id','salary','w_start_time'];

    public function position()
    {
        return $this->belongsTo(EmployeePosition::class);
    }


    public function superior()
    {
        return $this->belongsTo(Employee::class,'superior_id');
    }


    public function subordinated()
    {
        return $this->hasMany(Employee::class,'superior_id');
    }

    /**
     * Use that method instead of superior()
     * in order to get recursively all superiors above current employee
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subordinatedChain()
    {
        return $this->hasMany(Employee::class,'superior_id')->with('subordinatedChain');
    }
}
